CONTRIBUTION GUIDELINES
================================================================================

Updates and modifications
--------------------------------------------------------------------------------

If you are planning to update the plugin, follow these steps

* Annotate the changes into the CHANGELOG file
* Zip the entire folder, and upload the zippped file as: ACC-RemoteExporter.zip